<# =====================================================================
Auteur : MANANT Clément
Date : 19/10/2020
Description : affiche un résumé de l'OS
              liste les utilisateurs de la machine
              calcule et affiche le temps de réponse moyen vers 8.8.8.8 
======================================================================== #>

## Résumé de l'OS

# Affiche le nom de la machine
Write-Host ""
Write-host "Computer name : $env:COMPUTERNAME"

# Affiche l'IP principale
$ipaddress = ((ipconfig | findstr [0-9].\.)[0]).Split()[-1]
Write-host "Principal IP : $ipaddress"
Write-Host ""


# Affiche l'OS et la version de l'OS
$os = (Get-WmiObject -class Win32_OperatingSystem).Caption
$osversion = (Get-WmiObject -class Win32_OperatingSystem).Version
Write-host "OS : $os"
Write-host "OS version : $osversion"
Write-Host ""

# Affiche la date et l'heure de démarrage 
$date = (Get-CimInstance -ClassName Win32_OperatingSystem).LastBootUpTime
Write-host "Up since : $date"

# Détermine si l'OS est à jour
$maj = wmic OS get status
Write-Host "$maj"
Write-Host ""

# Affiche l'espace RAM utilisé / espace RAM disponible
$ramtotal = (Get-WmiObject -Class Win32_ComputerSystem).TotalPhysicalMemory/1GB
$ramdispo = (Get-WmiObject -Class Win32_OperatingSystem).FreePhysicalMemory/1MB
$ramuse = $ramtotal - $ramdispo
Write-Host "Ram :"
Write-Host "    Used : $ramuse GB"
Write-Host "    Free : $ramdispo GB"
Write-Host ""

# Affiche l'espace disque utilisé /espace disque disponible
$disquetotal = (Get-WmiObject Win32_LogicalDisk).Size/1GB
$disquedispo = (Get-WmiObject Win32_LogicalDisk).FreeSpace/1GB
$disqueuse = $disquetotal - $disquedispo
Write-Host "Disk :"
Write-Host "    Used : $disqueuse GB"
Write-Host "    Free : $disquedispo GB"
Write-Host ""

## Affiche la liste des utilisateurs de la machine
$users = (Get-WmiObject Win32_UserAccount).name
Write-Host "Users list : $users"
Write-Host ""

## Calcule et affiche le temps de réponse moyen vers 8.8.8.8
$ping = Test-Connection 8.8.8.8 -Count 4 -ea 0
if ($ping) {
    $averageping = ($ping | Measure-Object -Property ResponseTime -Average).Average
}
Write-Host "8.8.8.8 average ping time : $averageping ms"
Write-Host ""