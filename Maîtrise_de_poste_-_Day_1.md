# Maîtrise de poste - Day 1

## I. Self-footprinting

### Host OS

* Nom de la machine

```
PS C:\Users\cleme> $env:COMPUTERNAME
LAPTOP-TMEGJIHJ
```

* Os et version

OS :
```
PS C:\Users\cleme> (Get-WmiObject -class Win32_OperatingSystem).Caption
Microsoft Windows 10 Famille
```

Version :
```
PS C:\Users\cleme> (Get-WmiObject -class Win32_OperatingSystem).Version
10.0.18362
[...]
```

* Architecture processeur (32-bit, 64-bit, ARM, etc)

```
PS C:\Users\cleme> systeminfo
[...]
Type du système:                            x64-based PC
[...]
```

* Quantité RAM et modèle de la RAM

Quantité :
```
PS C:\Users\cleme> systeminfo
[...]
Mémoire physique totale:                    8 049 Mo
Mémoire physique disponible:                2 728 Mo
Mémoire virtuelle : taille maximale:        9 905 Mo
Mémoire virtuelle : disponible:             3 704 Mo
Mémoire virtuelle : en cours d’utilisation: 6 201 Mo
[...]
```

Modèle : 
```
PS C:\Users\cleme> wmic MemoryChip get /format:list
[...]
Model=
[...]
```

### Devices

* Marque et modèle du processeur 

```
PS C:\Users\cleme> Get-ComputerInfo
[...]
CsProcessors                                            : {Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz}
[...]
```

Nombre de processeurs et nombre de coeur :
```
PS C:\Users\cleme> WMIC CPU Get DeviceID,NumberOfCores,NumberOfLogicalProcessors
DeviceID  NumberOfCores  NumberOfLogicalProcessors
CPU0      4              8
```

Explication du nom : 
"Intel(R) Core(TM)" est la marque du processeur.
"i7" le nom du modèle.
"8" sa génération.
"550" sa référence.
"U" sa série.
"CPU" indique que c'est le microprocesseur principal de l'ordinateur.
"1.80 GHz" sa fréquence de base.

* Marque et modèle du trackpad/touchpad

```
PS C:\Users\cleme> Get-CimInstance win32_POINTINGDEVICE | select hardwaretype
hardwaretype
------------
Souris HID
```

* Marque et modèle de la carte graphique

```
PS C:\Users\cleme> wmic path win32_VideoController get name
Name
Intel(R) UHD Graphics 620
```

* Marque et modèle du disque dur

```
PS C:\Users\cleme> wmic diskdrive get model
Model
SanDisk SD9SN8W512G1002
```

* Différentes partitions du/des disque(s) dur(s)

```
PS C:\Users\cleme> wmic partition get name
Name
Disque n° 0, partition n° 0
Disque n° 0, partition n° 1
Disque n° 0, partition n° 2
```

* Système de fichier de chaque partition

```
PS C:\Users\cleme> Get-Volume
DriveLetter FriendlyName FileSystemType DriveType HealthStatus OperationalStatus SizeRemaining      Size
----------- ------------ -------------- --------- ------------ ----------------- -------------      ----
C           OS           NTFS           Fixed     Healthy      OK                    314.96 GB 475.89 GB
            RECOVERY     NTFS           Fixed     Healthy      OK                    364.64 MB    800 MB
```

* Explication de la fonction de chaque partition

La première partition est la partition système, elle permet de lancer le système.
La deuxième partition est la partition primaire, c'est la partition principale, utilisée par le système d'exploitation.
La dernière partition est la partition de récupération, elle permet de revenir à un point de départ s'il y a une erreur dans le système.

### Users

* Liste complète des utilisateurs de la machine

```
PS C:\Users\cleme> Get-LocalUser
Name               Enabled Description
----               ------- -----------
Administrateur     False   Compte d’utilisateur d’administration
cleme              True
DefaultAccount     False   Compte utilisateur géré par le système.
Invité             False   Compte d’utilisateur invité
WDAGUtilityAccount False   Compte d’utilisateur géré et utilisé par le système pour les scénarios Windows Defender A...
```

* Nom de l'utilisateur full admin

```
PS C:\Users\cleme> Get-LocalUser
Name               Enabled Description
----               ------- -----------
Administrateur     False   Compte d’utilisateur d’administration
[...]
```

### Processus

* Liste des processus de la machine 

```
PS C:\Users\cleme> Get-Process
Handles  NPM(K)    PM(K)      WS(K)     CPU(s)     Id  SI ProcessName
-------  ------    -----      -----     ------     --  -- -----------
    331      21     5404      16568       0,77  10156   7 ACMON
    392      23    14392      27192       0,38  14816   7 ApplicationFrameHost
    157       8     1752       6704             13848   0 AppVShNotify
    162       9     1848       6800       0,03  15188   7 AppVShNotify
    133      10     1660       3752              3832   0 AsLdrSrv
    152      11     1680        348       0,14  13236   7 ASUSHelloBG
    195      15     2556      10736       0,13  16836   7 ATKOSD2
   6070      84   157872     176536              3856   0 AvastSvc
   2175      53    30408      47772      17,86    496   7 AvastUI
    644      31    15536      29288       0,72   3012   7 AvastUI
    321      18     5528      20136       0,17  13088   7 backgroundTaskHost
    200      13     1904       8576       0,09  16336   7 BhcMgr
    168       8     2024       8048       0,08  10140   7 CompPkgSrv
    118       8     6508       5352       0,03   1028   7 conhost
    118       7     6420       3664              3952   0 conhost
    263      14     5264      15864       0,41   5372   7 conhost
    848      25     2160       4340               928   0 csrss
    710      21     2600       4928              8740   7 csrss
    421      16     4704      13904       2,58   8864   7 ctfmon
    464      23    12660      24044       1,00   1416   7 Discord
    964     110   246064     211260     112,52   2208   7 Discord
    781      45    34436      66920       9,89   7504   7 Discord
    249      18     9136      12624       0,09   7512   7 Discord
    687      30    70464      75380       6,66   8876   7 Discord
    331      22    10332      17176       0,17  11540   7 Discord
    204      17     3872       7976              9344   0 dllhost
    267      38     9416      17444       0,14  10348   7 dllhost
    123       7     1696       6040       0,13  13716   7 dllhost
    193      12     1724       6652       0,05  14284   7 DMedia
     81       6     1396       3936       0,25   2840   7 dptf_helper
   1075      39    80448      86648              1692   7 dwm
     59       6     1244       3364              4764   0 ELANFPService
    130       8     1796       4484              4756   0 esif_uf
   2440      80    54088     111188      28,61  16192   7 explorer
    914     105   346040     400492      58,92   2156   7 firefox
    441      31    27672      40588       0,17   3580   7 firefox
    738      60    60232      87944       4,47   3596   7 firefox
   1281     104   169432     213120      30,31   4424   7 firefox
    811      68   394000     418948      84,66   4892   7 firefox
    500      38    37276      60476       0,73   8216   7 firefox
    885      98   265064     289868      18,52   9228   7 firefox
    859      97   202480     234068      50,55  11128   7 firefox
    843      73    96988     124572       6,63  14620   7 firefox
   2113     138   249088     387176     104,31  16848   7 firefox
     32       5     1612       1756              1044   0 fontdrvhost
     32       7     2152       3916              8808   7 fontdrvhost
    585      34    23956      24676       0,66  11476   7 GiftBox.Agent
    227      12     2864       5048              4716   0 GiftBoxService
    244      13     2104       8296              2900   7 HControl
     89       5     1160       3212              4652   0 ibtsiva
   1441      11     2832       4928              4668   0 ICEsoundService64
      0       0       60          8                 0   0 Idle
    171      10     2056       6172              2736   0 igfxCUIService
    687      24     6720      21272       1,89   8896   7 igfxEM
    239      11     3508       8196       0,34   7812   7 igfxext
    165       9     2072       5648              4092   0 Intel_PIE_Service
    145       8     1484       4636              4620   0 IntelCpHDCPSvc
    136       7     1488       4368              5352   0 IntelCpHeciSvc
    158      10     1412       3408             14132   0 jhi_service
    279      15     3396       6352               376   0 LMS
    546      26    13500      39728       0,59   6784   7 LockApp
   1733      36     9552      18596               484   0 lsass
      0       0      500      96996              3112   0 Memory Compression
    222      15     2636       6884              5104   0 mfefire
    208      12     4484       7544              4660   0 mfemms
    138       9     1936       5080              5276   0 mfevtps
    280      12     6420      10012              5988   0 mfevtps
    973      51    54688       8440       2,56   6004   7 Microsoft.Photos
    215      13     2460        832             16540   0 MicrosoftEdgeUpdate
    946      45    34708      50528              4692   0 ModuleCoreService
    346      27    10144      21884       0,80   9128   7 ModuleCoreService
    222      14     3952       5996              4700   0 NvTelemetryContainer
    838      29    49544      56412              7260   0 OfficeClickToRun
    726      46    32432      16316              4644   0 OneApp.IGCC.WinService
    859      52    38168      46272      15,23  16788   7 OneDrive
    170       9     1704       5900              4676   0 PEFService
    658      33    64528      75200       0,88   5024   7 powershell
    313      15     5948        460       0,14  14580   7 RAVBg64
    364      15     4108        692       0,33  13720   7 RAVCpl64
      0      14     9460      42308               120   0 Registry
    136       8     1760       4396              4808   0 RstMwService
    141       9     1836       7156       0,17    264   7 RuntimeBroker
    383      22     8680      30252       1,03   1400   7 RuntimeBroker
    337      17     5356      21120       0,22   2492   7 RuntimeBroker
    321      18     6260      22728       2,34   2756   7 RuntimeBroker
    783      34    13316      43668       4,67   9488   7 RuntimeBroker
    405      18     4624      21496       1,09  10928   7 RuntimeBroker
    150       9     2208       7988       0,14  11500   7 RuntimeBroker
    386      19     5156      23568       5,73  12252   7 RuntimeBroker
    373      20     9640      30380       1,98  16552   7 RuntimeBroker
    722      74    30604      34768              9316   0 SearchIndexer
   1535     129   158524     234420      11,78   5908   7 SearchUI
    189      11     2288      12948       0,11  12100   7 SecurityHealthHost
    600      18     5164      11892             12092   0 SecurityHealthService
    152      10     1740       7696       0,08   5640   7 SecurityHealthSystray
    130       8     1524       4468              4684   0 servicehost
    798      12     6340       9376               472   0 services
    551      23     6696       6676       3,22  11344   7 SettingSyncHost
     89       7     4036       5616             13940   0 SgrmBroker
    787      33    28732      47736       2,86   9352   7 ShellExperienceHost
    671      19     7668      25152       6,89   5396   7 sihost
    541      33    46064      29312      82,86   8044   7 Skype
    850      47    31792      52368       6,59   8140   7 Skype
    258      18    12060      11804       0,14   8712   7 Skype
    429      56   122988      48560      22,58  11568   7 Skype
    370      22    13444      18108       0,28  16356   7 Skype
    407      23     8056      22372       0,13  16820   7 smartscreen
     53       3     1152        648               600   0 smss
    470      24     6112      12256              4268   0 spoolsv
    641      31    27860      68112       2,75   4944   7 StartMenuExperienceHost
     86       5      988       2424               548   0 svchost
    230      13     2844       8760              1036   0 svchost
   1352      26    17428      31360              1052   0 svchost
   1464      21    10392      15852              1196   0 svchost
    400      12     3692       8896              1256   0 svchost
    189      11     2224       5024              1504   0 svchost
    273      11     2824       7080              1512   0 svchost
    158      30     7416       8836              1568   0 svchost
    159       7     1824       4232              1576   0 svchost
    176      13     2004       5032              1616   0 svchost
    186      10     2084       8844              1672   0 svchost
    277      14     3132       8980              1680   0 svchost
    305      11     2656       7784              1768   0 svchost
    126       8     1668       4744              1804   0 svchost
    399      17     3216       8148              1820   0 svchost
    419      18     7256      13352              1832   0 svchost
    262      14     2756       8340              1844   0 svchost
    164      10     2376       9748              1856   0 svchost
    236      11     2316       7276              2144   0 svchost
    173       9     2124       6156              2160   0 svchost
    461      14    17576      15660              2224   0 svchost
    297      14     3928       6964              2244   0 svchost
    357      12     3136       8640              2304   0 svchost
    242      11     2888       6148              2332   0 svchost
    191      11     2160       5396              2412   0 svchost
    179       9     1864       5468              2448   0 svchost
    233      15     2404       7640              2480   0 svchost
    233      16     3372      11352              2512   0 svchost
    157      13     1812       5152              2600   0 svchost
    261       7     1356       3956              2660   0 svchost
    273      10     8408      15536              2768   0 svchost
    396      16     5336      10584              2788   0 svchost
    221      14     2288       6396              2948   0 svchost
    891      15     3636       7868              3228   0 svchost
    221      12     2436       7384              3268   0 svchost
    169      12     3164      12920              3372   0 svchost
    183       9     1816       5704              3380   0 svchost
    382      14     3644      11024              3500   0 svchost
    139      12     1888       4820              3616   0 svchost
    558      25     7340      15900              3676   0 svchost
    167       9     1840       7572       0,45   3772   7 svchost
    256      15     2976      11936              3844   0 svchost
    509      20     9100      24260       1,97   4160   7 svchost
    294      15     3820      10600              4332   0 svchost
    472      33    11268      14488              4368   0 svchost
    407      22    27728      35004              4628   0 svchost
    469     389    19104      21432              4636   0 svchost
    588      26    20928      29296              4728   0 svchost
    216      14     2124       5132              4784   0 svchost
    512      17    10260      17060              4800   0 svchost
    132       9     1632       3632              4840   0 svchost
    282      15     4632       9880              4852   0 svchost
    128       7     1324       3740              5068   0 svchost
    431      20     5392      18256              5076   0 svchost
    321      17     4252      13464              5292   0 svchost
    389      25     3564       8280              5420   0 svchost
    567      23     6016      11940              5456   0 svchost
    151       9     1844       6940              6712   0 svchost
    137       9     2164       4856              6772   0 svchost
    351      14     3612       8700              6844   0 svchost
    444      21     9868      16492              7016   0 svchost
    220      10     2096       7476              7096   0 svchost
    493      26     6572      18056              7348   0 svchost
    353      17     5400      19268              7396   0 svchost
    208       9     1872       5600              7580   0 svchost
    256      12     2988       7708              8376   0 svchost
    177      11     2248       7084              8384   0 svchost
    240      13     3120       9228              8392   0 svchost
    221      11     2336       7528              8604   0 svchost
    360      15     3500      12476              8616   0 svchost
    548      23     8724      31708       3,05   9340   7 svchost
    143       9     1696       4416              9660   0 svchost
    269      15     2908       6492              9744   0 svchost
    296      15     4140      13660             10424   0 svchost
    197      15     6588       7632             10692   0 svchost
    180      12     2572       8188             10820   0 svchost
    291      13     5372       8452             11248   0 svchost
    348      17     4256      21808       0,34  12304   7 svchost
    199      12     1772       7048             12656   0 svchost
    285      17     4880      13540             13880   0 svchost
    117       7     1372       4564             15448   0 svchost
    191      11     2268       7324             15460   0 svchost
    314      17     6392      12392             15776   0 svchost
    495      27     5764      20212       1,17  16392   7 svchost
   4618       0      208       3784                 4   0 System
    889      42    23584         20       1,69   5032   7 SystemSettings
    454      23     7236      26116       0,97  16332   7 SystemSettingsBroker
    289      31     6596      14936       0,72  11656   7 taskhostw
    514      22    12116      47560       0,84  17056   7 WindowsInternal.ComposableShell...
    165      11     1760       5092              1016   0 wininit
    277      12     2472       8844             17100   7 winlogon
    554      38    18092         36       1,06  12916   7 WinStore.App
    131       7     1856       4832              3940   0 wlanext
    278       9     3812       6980              2432   0 wsc_proxy
    310      12     2432       6560              1076   0 WUDFHost
    349      15     4556      10648              1320   0 WUDFHost
    725      48    25100       4164       1,17  13948   7 YourPhone
```

* Utilité de certains services systèmes 

"conhost" enregistre les entrées en console.

"lsass" se charge des politiques de sécurité (identification des utilisateurs Windows, changements de mot de passe...).

"RuntimeBroker" gére les permissions des applications du PC entre les applications Windows et s’assure qu’elles se comportent bien.

"svchost" fonctionne en tant qu'hôte pour d'autres processus.

"winlogon" gère l'ouverture et la fermeture de session Windows.

### Network

* Liste des cartes réseau

```
PS C:\Users\cleme> Get-NetAdapter | fl Name
Name : Connexion réseau Bluetooth 2

Name : Wi-Fi 2
```

La "Connexion réseau Bluetooth 2" permet de se connecter au réseau Bluetooth.
La "Wi-Fi 2" permet de se connecter au réseau Wi-Fi.

* Liste de tous les ports TCP et UDP en utilisation

```
PS C:\Users\cleme> netstat -ano
Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       1196
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:808            0.0.0.0:0              LISTENING       4644
  TCP    0.0.0.0:5040           0.0.0.0:0              LISTENING       7348
  TCP    0.0.0.0:5357           0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:7680           0.0.0.0:0              LISTENING       7016
  TCP    0.0.0.0:9001           0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING       484
  TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING       1016
  TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING       1832
  TCP    0.0.0.0:49667          0.0.0.0:0              LISTENING       2224
  TCP    0.0.0.0:49668          0.0.0.0:0              LISTENING       2948
  TCP    0.0.0.0:49669          0.0.0.0:0              LISTENING       4268
  TCP    0.0.0.0:49670          0.0.0.0:0              LISTENING       472
  TCP    127.0.0.1:6463         0.0.0.0:0              LISTENING       2208
  TCP    127.0.0.1:27275        0.0.0.0:0              LISTENING       3856
  TCP    127.0.0.1:54426        0.0.0.0:0              LISTENING       4716
  TCP    127.0.0.1:55694        127.0.0.1:55695        ESTABLISHED     4416
  TCP    127.0.0.1:55695        127.0.0.1:55694        ESTABLISHED     4416
  TCP    127.0.0.1:56113        127.0.0.1:56114        ESTABLISHED     14752
  TCP    127.0.0.1:56114        127.0.0.1:56113        ESTABLISHED     14752
  TCP    127.0.0.1:56201        127.0.0.1:56202        ESTABLISHED     11052
  TCP    127.0.0.1:56202        127.0.0.1:56201        ESTABLISHED     11052
  TCP    127.0.0.1:60659        127.0.0.1:60660        ESTABLISHED     16848
  TCP    127.0.0.1:60660        127.0.0.1:60659        ESTABLISHED     16848
  TCP    127.0.0.1:60661        127.0.0.1:60662        ESTABLISHED     14620
  TCP    127.0.0.1:60662        127.0.0.1:60661        ESTABLISHED     14620
  TCP    127.0.0.1:60666        127.0.0.1:60667        ESTABLISHED     9228
  TCP    127.0.0.1:60667        127.0.0.1:60666        ESTABLISHED     9228
  TCP    127.0.0.1:60670        127.0.0.1:60671        ESTABLISHED     3596
  TCP    127.0.0.1:60671        127.0.0.1:60670        ESTABLISHED     3596
  TCP    127.0.0.1:60672        127.0.0.1:60673        ESTABLISHED     8216
  TCP    127.0.0.1:60673        127.0.0.1:60672        ESTABLISHED     8216
  TCP    127.0.0.1:60695        127.0.0.1:60696        ESTABLISHED     11128
  TCP    127.0.0.1:60696        127.0.0.1:60695        ESTABLISHED     11128
  TCP    127.0.0.1:60741        127.0.0.1:60742        ESTABLISHED     2156
  TCP    127.0.0.1:60742        127.0.0.1:60741        ESTABLISHED     2156
  TCP    127.0.0.1:62407        127.0.0.1:62408        ESTABLISHED     9020
  TCP    127.0.0.1:62408        127.0.0.1:62407        ESTABLISHED     9020
  TCP    192.168.1.78:139       0.0.0.0:0              LISTENING       4
  TCP    192.168.1.78:49422     40.67.254.36:443       ESTABLISHED     5076
  TCP    192.168.1.78:55633     18.179.138.200:443     ESTABLISHED     16848
  TCP    192.168.1.78:55979     18.179.138.200:443     ESTABLISHED     16848
  TCP    192.168.1.78:56118     216.58.198.194:443     ESTABLISHED     16848
  TCP    192.168.1.78:56119     216.58.213.72:443      TIME_WAIT       0
  TCP    192.168.1.78:56122     216.58.198.194:443     TIME_WAIT       0
  TCP    192.168.1.78:56123     172.217.23.67:443      ESTABLISHED     16848
  TCP    192.168.1.78:56125     216.58.213.68:443      TIME_WAIT       0
  TCP    192.168.1.78:56128     216.58.213.68:443      ESTABLISHED     16848
  TCP    192.168.1.78:56130     216.58.201.227:443     TIME_WAIT       0
  TCP    192.168.1.78:56131     216.58.213.129:443     ESTABLISHED     16848
  TCP    192.168.1.78:56132     216.58.213.162:443     ESTABLISHED     16848
  TCP    192.168.1.78:56133     216.58.201.238:443     ESTABLISHED     16848
  TCP    192.168.1.78:56134     216.58.209.226:443     ESTABLISHED     16848
  TCP    192.168.1.78:56135     216.58.198.194:443     ESTABLISHED     16848
  TCP    192.168.1.78:56136     142.250.74.238:443     ESTABLISHED     16848
  TCP    192.168.1.78:56138     172.217.22.131:443     TIME_WAIT       0
  TCP    192.168.1.78:56139     142.250.74.227:443     TIME_WAIT       0
  TCP    192.168.1.78:56140     216.58.213.142:443     ESTABLISHED     16848
  TCP    192.168.1.78:56146     149.171.60.221:443     TIME_WAIT       0
  TCP    192.168.1.78:56153     51.138.106.75:443      TIME_WAIT       0
  TCP    192.168.1.78:56155     216.58.209.226:443     ESTABLISHED     16848
  TCP    192.168.1.78:56159     188.165.53.185:443     ESTABLISHED     16848
  TCP    192.168.1.78:56160     92.123.236.162:80      ESTABLISHED     16848
  TCP    192.168.1.78:56161     192.0.76.3:443         ESTABLISHED     16848
  TCP    192.168.1.78:56162     216.58.201.234:443     ESTABLISHED     16848
  TCP    192.168.1.78:56164     216.58.206.234:443     ESTABLISHED     16848
  TCP    192.168.1.78:56165     157.240.21.16:443      ESTABLISHED     16848
  TCP    192.168.1.78:56166     192.0.77.2:443         ESTABLISHED     16848
  TCP    192.168.1.78:56168     216.58.214.67:443      ESTABLISHED     16848
  TCP    192.168.1.78:56170     216.58.201.234:80      ESTABLISHED     16848
  TCP    192.168.1.78:56176     216.58.209.226:80      ESTABLISHED     16848
  TCP    192.168.1.78:56183     216.58.201.226:443     ESTABLISHED     16848
  TCP    192.168.1.78:56184     79.98.96.110:80        ESTABLISHED     16848
  TCP    192.168.1.78:56185     192.0.73.2:80          TIME_WAIT       0
  TCP    192.168.1.78:56188     157.240.21.35:443      ESTABLISHED     16848
  TCP    192.168.1.78:56190     87.98.224.187:443      ESTABLISHED     16848
  TCP    192.168.1.78:56191     192.124.249.23:80      ESTABLISHED     16848
  TCP    192.168.1.78:56193     216.58.209.226:443     ESTABLISHED     16848
  TCP    192.168.1.78:56194     172.217.19.238:443     ESTABLISHED     16848
  TCP    192.168.1.78:56195     216.58.213.162:443     ESTABLISHED     16848
  TCP    192.168.1.78:56196     216.58.204.130:443     ESTABLISHED     16848
  TCP    192.168.1.78:56197     216.58.204.97:443      ESTABLISHED     16848
  TCP    192.168.1.78:56198     142.250.74.227:443     ESTABLISHED     16848
  TCP    192.168.1.78:56199     216.58.209.226:443     ESTABLISHED     16848
  TCP    192.168.1.78:56200     192.0.77.2:443         ESTABLISHED     16848
  TCP    192.168.1.78:56203     10.33.18.87:7680       SYN_SENT        7016
  TCP    192.168.1.78:56204     213.186.33.2:80        ESTABLISHED     16848
  TCP    192.168.1.78:56205     51.138.106.75:443      ESTABLISHED     4160
  TCP    192.168.1.78:58449     152.199.19.161:443     CLOSE_WAIT      5908
  TCP    192.168.1.78:60027     5.62.53.11:443         ESTABLISHED     3856
  TCP    192.168.1.78:60261     40.67.251.132:443      ESTABLISHED     16788
  TCP    192.168.1.78:60649     162.159.136.234:443    ESTABLISHED     1416
  TCP    192.168.1.78:60655     5.45.62.117:80         ESTABLISHED     3856
  TCP    192.168.1.78:60677     44.237.16.197:443      ESTABLISHED     16848
  TCP    192.168.1.78:60691     13.107.18.11:443       ESTABLISHED     16848
  TCP    [::]:135               [::]:0                 LISTENING       1196
  TCP    [::]:445               [::]:0                 LISTENING       4
  TCP    [::]:808               [::]:0                 LISTENING       4644
  TCP    [::]:5357              [::]:0                 LISTENING       4
  TCP    [::]:7680              [::]:0                 LISTENING       7016
  TCP    [::]:9001              [::]:0                 LISTENING       4
  TCP    [::]:49664             [::]:0                 LISTENING       484
  TCP    [::]:49665             [::]:0                 LISTENING       1016
  TCP    [::]:49666             [::]:0                 LISTENING       1832
  TCP    [::]:49667             [::]:0                 LISTENING       2224
  TCP    [::]:49668             [::]:0                 LISTENING       2948
  TCP    [::]:49669             [::]:0                 LISTENING       4268
  TCP    [::]:49670             [::]:0                 LISTENING       472
  TCP    [::1]:27275            [::]:0                 LISTENING       3856
  TCP    [::1]:49943            [::]:0                 LISTENING       14132
  UDP    0.0.0.0:123            *:*                                    12656
  UDP    0.0.0.0:3702           *:*                                    9744
  UDP    0.0.0.0:3702           *:*                                    9744
  UDP    0.0.0.0:5050           *:*                                    7348
  UDP    0.0.0.0:5353           *:*                                    2244
  UDP    0.0.0.0:5355           *:*                                    2244
  UDP    0.0.0.0:49668          *:*                                    3856
  UDP    0.0.0.0:59434          *:*                                    9744
  UDP    127.0.0.1:1900         *:*                                    4784
  UDP    127.0.0.1:49664        *:*                                    5456
  UDP    127.0.0.1:49666        *:*                                    3856
  UDP    127.0.0.1:51231        *:*                                    4784
  UDP    192.168.1.78:137       *:*                                    4
  UDP    192.168.1.78:138       *:*                                    4
  UDP    192.168.1.78:1900      *:*                                    4784
  UDP    192.168.1.78:51230     *:*                                    4784
  UDP    [::]:123               *:*                                    12656
  UDP    [::]:3702              *:*                                    9744
  UDP    [::]:3702              *:*                                    9744
  UDP    [::]:5353              *:*                                    2244
  UDP    [::]:5355              *:*                                    2244
  UDP    [::]:59435             *:*                                    9744
  UDP    [::1]:1900             *:*                                    4784
  UDP    [::1]:51229            *:*                                    4784
  UDP    [fe80::24af:3d3e:6989:7f04%3]:1900  *:*                                    4784
  UDP    [fe80::24af:3d3e:6989:7f04%3]:51228  *:*                                    4784
```

## II. Scripting 

## III. Gestion des softs

Un gestionnaire de paquet permet l'installation de paquets à partir de l'invite de commande. Tous les paquets sont stockés dans le même dépôt. Cela permet à l'utilisateur d'avoir directement accès aux paquets à installer. Le téléchargement se fait directement depuis le site demandé, il n'y a pas d'autre intervenant tel que le moteur de recherche. Ces paquets sont dépouvus de spywares et de malwares, ce qui assure la sécurité de ces derniers.

Liste des paquets : 
```
PS C:\WINDOWS\system32> choco list --localonly
Chocolatey v0.10.15
chocolatey 0.10.15
chocolatey-core.extension 1.3.5.1
2 packages installed.
```

Source des paquets : 
```
PS C:\WINDOWS\system32> choco source
Chocolatey v0.10.15
chocolatey - https://chocolatey.org/api/v2/ | Priority 0|Bypass Proxy - False|Self-Service - False|Admin Only - False.
```

## IV. Machine virtuelle

### 3. Configuration post-install

Liste des cartes réseau : 
```
[cleme@localhost partage]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:38:46:b1 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global noprefixroute dynamic enp0s3
       valid_lft 66955sec preferred_lft 66955sec
    inet6 fe80::df0d:c17b:1908:c06b/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:19:4d:a7 brd ff:ff:ff:ff:ff:ff
    inet 192.168.120.50/24 brd 192.168.120.255 scope global noprefixroute dynamic enp0s8
       valid_lft 403sec preferred_lft 403sec
    inet6 fe80::69c9:2368:4c5c:b4ec/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

4. Partage de fichiers

Création d'un dossier pour accéder au partage : 
```
[cleme@localhost ~]$ sudo mkdir /opt/partage
```

Monte du partage dans la VM : 
```
[cleme@localhost ~]$ sudo mount -t cifs -o username=toto,password=pass //192.168.1.78/share /opt/partage
[sudo] password for cleme:
[cleme@localhost ~]$ df -h
Filesystem               Size  Used Avail Use% Mounted on
devtmpfs                 484M     0  484M   0% /dev
tmpfs                    496M     0  496M   0% /dev/shm
tmpfs                    496M   13M  483M   3% /run
tmpfs                    496M     0  496M   0% /sys/fs/cgroup
/dev/mapper/centos-root  6.2G  1.3G  5.0G  21% /
/dev/sda1               1014M  137M  878M  14% /boot
tmpfs                    100M     0  100M   0% /run/user/0
tmpfs                    100M     0  100M   0% /run/user/1000
//192.168.1.78/share     476G  321G  156G  68% /opt/partage
```

Accès au fichier de partage et création d'un fichier vide depuis la VM : 
```
[cleme@localhost ~]$ cd /opt/partage/
[cleme@localhost partage]$ sudo touch toto
[cleme@localhost partage]$
```

