<# =====================================================================
Auteur : MANANT Clément
Date : 31/10/2020
Description : permet de lock l'écran après X secondes
              ou d'éteindre le PC après X secondes
======================================================================== #>

# Vérifie si le nombre d'argument est correct
if ($args.Count -eq 2){

    # Lock l'écran après X secondes
    if ($args[0] -eq "lock") {

        if ($args[1] -is [int]) {

            Start-Sleep -Seconds $args[1]
            rundll32.exe user32.dll,LockWorkStation
        }
        Else {

            Write-Host "Second argument incorrect, it's not an integer"
        }
    }

    # Eteinds l'ordi après X secondes
    ElseIf ($args[0] -eq "shutdown") {

        if ($args[1] -is [int]) {

            Start-Sleep -Seconds $args[1]
            Stop-Computer
        }

        Else {

            Write-Host "Second argument incorrect, it's not an integer"
        }
    }

    Else {

        Write-Host "Argument(s) incorrect(s), the only possible commands are lock X and shutdown X (X is the time to wait in seconds before the command is executed)."
    }
}

Else {

    Write-Host "Argument's number is incorrect, the only possible commands are lock X and shutdown X (X is the time to wait in seconds before the command is executed)."
}
